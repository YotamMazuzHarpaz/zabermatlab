classdef ZaberRotary < ZaberActuator
% Copyright (c) 2015-2016 Yotam Mazuz-Harpaz, The Hebrew University of Jerusalem.
% Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
% The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    % This class is a wrapping interface for a direct and easy usage of Zaber Rotary devices.
	% Initially programmed and tested with a rotary stage of model T-RSW60C.
    
    properties (Constant)
        FullRoundPos = 384000;
        MinAngle = 0;
        MaxAngle = 360;
    end
    
    methods
        %% Infrastructure functions
        
        % Constructor
        function this = ZaberRotary(Port, DeviceId)
            % Call parent class's constructor
            this@ZaberActuator(Port, DeviceId);
        end
        
        % Conversion functions Position<->Angle
        function Deg = Pos2Deg(this, Pos)
           Deg = 360*Pos/this.FullRoundPos;
        end
        function Pos = Deg2Pos(this, Deg)
           Pos = floor(Deg*this.FullRoundPos/360);
        end       
        
        %% Motion functions
        
        % Send device to a specified angle
        function FinalAngle = GotoAngle(this, Angle)
            if Angle<this.MinAngle || Angle>this.MaxAngle
                error('Requested angle out of range')
            end
            
            Pos = this.Deg2Pos(Angle);
            FinalPos = this.MoveAbsolute(Pos);
            FinalAngle = this.Pos2Deg(FinalPos);
        end
        
        % Query current angle
        function Angle = GetAngle(this)
            Pos = this.GetPosition();
            Angle = this.Pos2Deg(Pos);
        end
        
    end
end

