% This script demonstrates usage of the ZaberActuator class.

% Device connection parameters
port = 'com7';
deviceId = 1;

path(path, '..') % Assuming the class definition file is in the parent directory
zaberHdl = ZaberActuator(port, deviceId); % Create object
zaberHdl.Connect() % Connect to device (plots true if succeeded)
zaberHdl.Home(); % Go home
pos1 = zaberHdl.GetPosition() % Get position (expected to be 0)
zaberHdl.MoveRelative(200, 'synch'); % Move 200 steps, wait for jurney to complete
pos2 = zaberHdl.GetPosition() % Get new position (expected to be 200)
zaberHdl.Close();