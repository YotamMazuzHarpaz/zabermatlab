classdef ZaberActuator < handle
% Copyright (c) 2015-2016 Yotam Mazuz-Harpaz, The Hebrew University of Jerusalem.
% Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
% The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    % This class is a wrapping interface for a direct and easy usage of a Zaber Actuator.
	% Initially programmed and tested with actuator of model T-NA08A50.
	
    properties (Constant, Access=protected) % Constant and hidden properties
        % Connection settings
        BaudRate = 9600;
        % Command codes
        command_home = 1;
        command_store_current_position = 16;
        command_return_stored_position = 17;
        command_move_to_stored_position = 18;
        command_move_absolute = 20;
        command_move_relative = 21;
        command_const_velocity = 22;
        command_stop = 23;
        command_target_speed = 42;
        command_set_maximum_position = 44;
        command_return_setting = 53;
        command_return_status = 54;
        command_echo_data = 55;
        command_current_position = 60;
        command_set_minimum_position = 106;
        command_error_code = 255;
    end
    
    properties (SetAccess = private)
        % Identity settings
        Port;
        SerialHdl;
        DeviceId;
        % Connection settings
        Connected;
        TimeOut = 5;
        % Control settings
        LastStop = tic();
    end
    
    methods (Static = true)
        %% Communication infrastructure - conversion between bytes and numbers util. functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Convert the 4-bytes representation structure, used by the device
        % interface, into a single signed integer
        function Data = Bytes2Number(Bytes)
            % Ensamble a single integer from the bytes struture
            Data = 0; 
            for i=1:4
                Data = Data + 256^(i-1) * Bytes(i);
            end
            % Handle negative values
            if Bytes(4) > 127
                Data = Data - 256^4;
            end
        end
       
        % Fit a command data into the 4-bytes structure required by the device interface       
       function DataInBytes = Number2Bytes(data)             
             % Handle negative values
            if data<0
                data = 256^4 + data;
            end
            % Break integer into bytes
            for i=4:-1:1
                DataInBytes(i) = floor(data / 256^(i-1));
                data = data - 256^(i-1) * DataInBytes(i);
                if DataInBytes(i) > 256
                    if i==1
                        error('Overflow occurred');
                    else
                        DataInBytes(i) = 256;
                    end
                end
            end
        end 
    end
    
    methods
        %% Connection infrastructure
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Constructor
        function this = ZaberActuator(Port, DeviceId)
            % Set the port and device ID
            if (~exist('Port', 'var'))
                error('Please specify port');
            end
            this.Port = Port;
            if (~exist('DeviceId', 'var'))
                error('Please specify device id');
            end
            this.DeviceId = DeviceId;
            this.Connected = false;
        end
        
        % Connect to the device
        function Connected = Connect (this)
            % Check if the port is already defined
            s = instrfind('Type', 'serial', 'Port', this.Port);
            OpenFound = false;
            if ~isempty(s)
                % If there are existing handles to this port, search for an open one
                for i1=1:length(s)
                    if strcmp(s(i1).status, 'open') && s(i1).baudrate==this.BaudRate
                        this.SerialHdl = s(i1);
                        OpenFound = true;
                        break;
                    elseif strcmp(s(i1).status, 'open') && s(i1).baudrate~=this.BaudRate
                        error('Defined port is already open, with different baudrate');
                    end
                end
            end
            
            % If no open handle found, create the handle and open the port
            if ~OpenFound
                this.SerialHdl = serial(this.Port,'BaudRate',this.BaudRate);
                fopen(this.SerialHdl);
            end
                        
            % Test connection
            SendCode = randi(100);
            GetCode = this.SendSyncCommand(this.command_echo_data, SendCode);
            if GetCode == SendCode
                this.Connected = true;
            else
                this.Connected = false;
            end
            % Return result
            Connected = this.Connected;
        end
           
        % Dummy read from buffer, just to clear it
        function ClearBuffer(this)
            % This may be redundant, but I'm not sure if there's a difference between the two actions:
            flushinput(this.SerialHdl); % 1.
            while this.SerialHdl.BytesAvailable > 0 % 2.
                fread(this.SerialHdl,6);
            end
        end
        
        % Close connection
        function Close(this)
            % That method may be a source to clashes in case that several actuators are connected to the same port
            % (typically, in the case of a daisy-chain), since it closes the port regardless of other objects dependence on it.
            if isvalid(this.SerialHdl) && strcmp(this.SerialHdl.status, 'open')
                fclose(this.SerialHdl);
            end
            if ~isempty(this.SerialHdl) || ~isvalid(this.SerialHdl)
                delete(this.SerialHdl);
            end
            this.Connected = false;
        end
        
        % Query connection status
        function response = IsConnected(this)
            response = this.Connected;
        end
    end
    
    %% Connection infrastructure - private functions (force user to use the specific wrapper functions)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Access = private)
       % Send command to the device
        function SendCommand(this, CommandId, Parameter)            
            % Set command data
            if (~exist('Parameter', 'var'))
                DataInBytes = zeros(1,4);
            else
                DataInBytes = this.Number2Bytes(Parameter);
            end
            % Ensemble command
            Command = [this.DeviceId, CommandId, DataInBytes];
            fwrite(this.SerialHdl,Command);
        end
        
        % Send command and wait for response
        function Response = SendSyncCommand(this, CommandId, Parameter)
            % Clear buffer, to get the expected response
            this.ClearBuffer();
            % Send command
            if (~exist('Parameter', 'var'))
                SendCommand(this, CommandId);
            else
                SendCommand(this, CommandId, Parameter);
            end
            % And wait for response
            Response = GetResponse(this);
        end 
        
        % Wait for response from the device, after sending a synchronized command
        function Response = GetResponse(this)
            t0 = tic();
            IsTimeout = false;
            % Wait for response
            while this.SerialHdl.BytesAvailable < 6 && ~IsTimeout && this.LastStop<t0
                pause(0.01);
                t = toc(t0);
                % If we're waiting pass the timeout definition, check if the device is in motion.
                % If it is, keep waiting. Otherwise, exit with a 'timeout'
                if t-t0 > this.TimeOut
                   if this.IsInMotion()
                       pause(0.1);
                       IsTimeout = false;
                   else
                       IsTimeout = true;
                   end
                end
            end
            
            % If no timeout occurred and no stop commanded, get device's response
            if ~IsTimeout && this.LastStop<t0
                ResponseBytes = fread(this.SerialHdl,6);
                CommandID = ResponseBytes(2);
                DataBytes = ResponseBytes(3:6);
                % Translate response from the 4-bytes set to a single readable integer
                Response = this.Bytes2Number(DataBytes);
            else
                Response = [];
                if IsTimeout
                    warning('Time out occurred')
                end
            end
            
            if CommandID == this.command_error_code
                Response = [];
                warning('Device raised an error'); 
            end
        end
        
    end
    
    methods
        %% Motion commands
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Check if the device is in motion
        function reply = IsInMotion(this)
            status = SendSyncCommand(this, this.command_return_status);
            reply = (status~=0);
        end
        
        % Go home (maximum retraction)
        function Home(this, Mode)
            % Mode is an optional parameter. Set it to 'asynch'/'synch'(default) to
            % choose between sending the command synchronously/asynchronously. 
            if ~exist('Mode', 'var')
                Mode = 'synch';
            elseif all(~strcmpi(Mode, {'synch', 'asynch'}))
                warning('Unidentified call mode. Calling synchronously as default')
                Mode = 'synch';
            end
            % Send command in the required mode (synchronous/asynchronous)
            if strcmpi(Mode, 'synch')
                SendSyncCommand(this, this.command_home);
            else
                this.SendCommand(this.command_home);
            end
        end
        
        % Query min and max positions
        function MaxPos = GetMaxPosition(this)
            MaxPos = this.SendSyncCommand(this.command_return_setting, this.command_set_maximum_position);
        end
            % Note that not all devices support this command
        function MinPos = GetMinPosition(this)
            MinPos = this.SendSyncCommand(this.command_return_setting, this.command_set_minimum_position);
        end
        
        % Stop motion
        function Stop(this)
           this.LastStop = tic();
           this.SendCommand(this.command_stop);
        end
        
        % Query device's position
        function Position = GetPosition(this)
           Position = this.SendSyncCommand(this.command_current_position);
        end
        
        % Relative movement
        function FinalPosition = MoveRelative(this, Step, Mode)
            % Mode is an optional parameter. Set it to 'asynch'/'synch'(default) to
            % choose between sending the command synchronously/asynchronously. 
            if ~exist('Mode', 'var')
                Mode = 'synch';
            elseif all(~strcmpi(Mode, {'synch', 'asynch'}))
                warning('Unidentified call mode. Calling synchronously as default')
                Mode = 'synch';
            end
            % Send command in the required mode (synchronous/asynchronous)
            if strcmpi(Mode, 'synch')
                FinalPosition = this.SendSyncCommand(this.command_move_relative, Step);
            else
                this.SendCommand(this.command_move_relative, Step);
                FinalPosition = []; % We're not waiting for a response, final position is not determined yet
            end 
        end
        
        
        % Constant velocity
        function ConstantVelocity(this, Speed, Mode)
            % Mode is an optional parameter. Set it to 'asynch'/'synch'(default) to
            % choose between sending the command synchronously/asynchronously. 
            if ~exist('Mode', 'var')
                Mode = 'synch';
            elseif all(~strcmpi(Mode, {'synch', 'asynch'}))
                warning('Unidentified call mode. Calling synchronously as default')
                Mode = 'synch';
            end
            % Send command in the required mode (synchronous/asynchronous)
            if strcmpi(Mode, 'synch')
                this.SendSyncCommand(this.command_const_velocity, Speed);
            else
                this.SendCommand(this.command_const_velocity, Speed);
            end 
        end
        
        
        % Absolute movement
        function FinalPosition = MoveAbsolute(this, Position, Mode)
            % Mode is an optional parameter. Set it to 'asynch'/'synch'(default) to
            % choose between sending the command synchronously/asynchronously. 
            if ~exist('Mode', 'var')
                Mode = 'synch';
            elseif all(~strcmpi(Mode, {'synch', 'asynch'}))
                warning('Unidentified call mode. Calling synchronously as default')
                Mode = 'synch';
            end
            % Send command in the required mode (synchronous/asynchronous)
            if strcmpi(Mode, 'synch')
                FinalPosition = this.SendSyncCommand(this.command_move_absolute, Position);
            else
                this.SendCommand(this.command_move_absolute, Position);
                FinalPosition = []; % We're not waiting for a response, final position is not determined yet
            end 
        end
        
        % Move to a stored position
        function FinalPosition = MoveToStoredPosition(this, Address, Mode)
            % Mode is an optional parameter. Set it to 'asynch'/'synch'(default) to
            % choose between sending the command synchronously/asynchronously. 
            if ~exist('Mode', 'var')
                Mode = 'synch';
            elseif all(~strcmpi(Mode, {'synch', 'asynch'}))
                warning('Unidentified call mode. Calling synchronously as default')
                Mode = 'synch';
            end
            % Send command in the required mode (synchronous/asynchronous)
            if strcmpi(Mode, 'synch')
                FinalPosition = this.SendSyncCommand(this.command_move_to_stored_position, Address);
            else
                this.SendCommand(this.command_move_to_stored_position, Address);
                FinalPosition = []; % We're not waiting for a response, final position is not determined yet
            end
        end
        
        % Store position
        function result = StoreCurrentPosition(this, Address)
            Reply = this.SendSyncCommand(this.command_store_current_position, Address);
            if Reply==Address
                result = 1;
            else
                result = 0;
                warning('Storing actuator''s position failed');
            end
        end
        
        % Query position stored in one of the buffers
        function Position = ReturnStoredPosition(this, Address)
            Position = this.SendSyncCommand(this.command_return_stored_position, Address);
        end
    end
end